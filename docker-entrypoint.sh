#!/bin/sh

echo "starting docker-entrypoint.sh"
echo "$(date)"
echo "hubzilla version: ${HUBZILLAVERSION}"
echo "domain: ${HUBZILLADOMAIN}"

echo "checking ${DATADIR}/config/.htconfig.php"
if [ ! -e "${DATADIR}/config/.htconfig.php" ]; then
  # link /var/www/hubzilla/.htconfig.php -> /data/config/.htconfig.php from Dockerfile
  mkdir -p "${DATADIR}/config"
  touch "${DATADIR}/config/.htconfig.php"
fi

echo "checking ${DATADIR}/logs/.htlog"
if [ ! -e "${DATADIR}/logs/.htlog" ]; then
  # link /var/www/hubzilla/.htlog -> /data/logs/.htlog from Dockerfile
  mkdir -p "${DATADIR}/logs"
  touch "${DATADIR}/logs/.htlog"
fi

echo "checking /data/store"
if [ ! -e ${DATADIR}/store ]; then
  # link /var/www/hubzilla/store -> /data/store from Dockerfile
  mkdir -p "${DATADIR}/store"
  mkdir -p "${DATADIR}/store/[data]/smarty3"
fi

echo "configure nginx"
sed s/###HUBZILLADOMAIN###/${HUBZILLADOMAIN}/ /etc/nginx/templates/default.conf > /etc/nginx/conf.d/default.conf

echo "setting correct permissions on ${DATADIR}"
# php-fpm is running as ${PHPFPMUSER}:${PHPFPMGROUP}
chown -R ${PHPFPMUSER}:${PHPFPMGROUP} "${DATADIR}"
find "${DATADIR}" -type d -exec chmod 755 {} \;
find "${DATADIR}" -type f -exec chmod 644 {} \;
# /var/www/hubzilla//store/[data]/smarty3 must be writable by webserver
#find "${DATADIR}/store/[data]/smarty3" -type d -exec chmod 777 {} \;
#find "${DATADIR}/store/[data]/smarty3" -type f -exec chmod 666 {} \;

echo "setting correct permission to /var/tmp/nginx"
# bug with tmpfs?
chmod 755 /var/tmp/nginx

echo "installing crontab"
echo "*/${HUBZILLAINTERVAL} * * * * cd "${HUBZILLADIR}"; /usr/bin/php Zotlabs/Daemon/Master.php Cron > /dev/stdout 2>&1" | crontab -u ${PHPFPMUSER} -

echo "checking db"
if [ -n $PGDATABASE ]; then
  echo "using postgres"
  if [ "$(psql -c '\dt')" = "Did not find any relations." ]; then
    echo "Import schema";
    psql -f /var/www/hubzilla/install/schema_postgres.sql;
  fi
fi

echo "starting php-fpm7"
php-fpm7

echo "starting nginx"
nginx -g "daemon off;"

echo "stopping docker-entrypoint.sh"
echo "$(date)"
echo "hubzilla version: ${HUBZILLAVERSION}"
echo "domain: ${HUBZILLADOMAIN}"
