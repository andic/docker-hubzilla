FROM alpine:3.8
MAINTAINER Andreas Cislik <git@a11k.net>

ENTRYPOINT ["/docker-entrypoint.sh"]
VOLUME /data

COPY nginx-default.conf /etc/nginx/templates/default.conf
COPY php-fpm-hubzilla.conf /etc/php7/php-fpm.d/hubzilla.conf
COPY docker-entrypoint.sh /docker-entrypoint.sh

ENV HUBZILLAVERSION 4.0
ENV HUBZILLADOMAIN hub.example.org
ENV HUBZILLAINTERVAL 10
ENV HUBZILLADIR /var/www/hubzilla
ENV DATADIR /data
  
ENV NGINXUSER nginx
ENV NGINXGROUP nginx
ENV PHPFPMUSER www-data
ENV PHPFPMGROUP www-data
ENV HUBZILLAADDONS nsfw superblock phpmailer


RUN set -ex \
  && apk update \
  && apk upgrade \
  && apk add \
    bash \
    curl \
    dcron \
    ffmpeg \
    gd \
    git \
    imagemagick \
    nginx \
    openssl \
    php7 \
    php7-ctype \
    php7-curl \
    php7-dom \
    php7-fpm \
    php7-gd \
    php7-imagick \
    php7-json \
    php7-mbstring \
    php7-mcrypt \
    php7-openssl \
    php7-pdo_mysql \
    php7-pdo_pgsql \
    php7-session \
    php7-xml \
    php7-zip \
    postgresql-client \
  && addgroup ${PHPFPMGROUP} || echo "group ${PHPFPMGROUP} exists" \
  && adduser -D -G ${PHPFPMGROUP} ${PHPFPMUSER} || echo "user ${PHPFPMUSER} exists" \
  && mkdir -p /run/nginx \
  && mkdir -p ${HUBZILLADIR} \
  && cd ${HUBZILLADIR} \
  && curl https://framagit.org/hubzilla/core/-/archive/${HUBZILLAVERSION}/core-${HUBZILLAVERSION}.tar.gz | tar -xz --strip-components=1 -C ${HUBZILLADIR} \
  && util/add_addon_repo https://framagit.org/hubzilla/addons.git hzaddons \
  && util/add_theme_repo https://github.com/DeadSuperHero/hubzilla-themes.git dshthemes insecure \
  && util/add_theme_repo https://framagit.org/disroot/hubzilla/dishub-themes.git disthemes insecure \
  && util/add_theme_repo https://github.com/BOETZILLA/BLUEBASIC_Theme boetthemes insecure \
  && util/importdoc \
  && chown -R ${NGINXUSER}:${NGINXGROUP} ${HUBZILLADIR} \
  && find ${HUBZILLADIR} -type d -exec chmod 755 {} \; \
  && find ${HUBZILLADIR} -type f -exec chmod 644 {} \; \
  && sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php7/php.ini \
  && sed -i 's#;error_log = log/php7/error.log#error_log = /dev/stdout#' /etc/php7/php-fpm.conf \
  && sed -i 's/###PHPFPMUSER###/${PHPFPMUSER}/' /etc/php7/php-fpm.d/hubzilla.conf \
  && sed -i 's/###PHPFPMGROUP###/${PHPFPMGROUP}/' /etc/php7/php-fpm.d/hubzilla.conf \
  && sed -i 's#error_log /var/log/nginx/error.log warn;#error_log /dev/stdout warn;#' /etc/nginx/nginx.conf \
  && sed -i 's#access_log /var/log/nginx/access.log main;#access_log /dev/stdout main;#' /etc/nginx/nginx.conf \
  && mv /etc/php7/php-fpm.d/www.conf /etc/php7/php-fpm.d/www.conf.org \
  && chmod u+x /docker-entrypoint.sh \
  && ln -sf ${DATADIR}/config/.htconfig.php ${HUBZILLADIR}/.htconfig.php \
  && ln -sf ${DATADIR}/logs/.htlog ${HUBZILLADIR}/.htlog \
  && if [ -e ${HUBZILLADIR}/store ]; then mv ${HUBZILLADIR}/store ${HUBZILLADIR}/store.org; fi \
  && ln -sf ${DATADIR}/store ${HUBZILLADIR}/store \
  && echo "done building from Dockerfile"
  